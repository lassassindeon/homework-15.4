#include <iostream>

int Odd(int b)
{
	for (int a = 0; a < b; ++a)
	{
		std::cout << a++ << " - Is Odd" << '\n';
	}
	return(b);
}

int NotOdd(int b)
{
	for (int a = 0; a < b; ++a)
	{
		std::cout << ++a << " - Not Odd" << '\n';
	}
	return(b);
}